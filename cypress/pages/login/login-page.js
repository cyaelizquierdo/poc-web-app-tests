class LoginPage {
  constructor() {
    this.url = '/login';
  }

  // Elements
  ohanaLogo() { return cy.get('div#col-1>div>img'); }

  emailLabel() { return cy.get('label[class="form-label"]:first'); }

  emailInput() { return cy.get('[data-cy="email"]'); }

  passwordLabel() { return cy.get('label[class="form-label"]:eq(1)'); }

  recoveredPasswordLabel() { return cy.get('a.small'); }

  passwordInput() { return cy.get('[data-cy="password"]'); }

  seePasswordButton() { return cy.get('span.pointer'); }

  loginButton() { return cy.get('[data-cy="btn-submit"]'); }

  downloadFirstLabel() { return cy.get('div.text-center>p:nth-child(1)'); }

  downloadSecondLabel() { return cy.get('div.text-center>p:nth-child(2)'); }

  downloadAndroidButton() { return cy.get('div.app-download:eq(0)'); }

  downloadIosButton() { return cy.get('div.app-download:eq(1)'); }

  visit() {
    cy.visit(this.url);
  }

  login(email, password) {
    this.emailInput().type(email);
    this.passwordInput().type(password);
    this.loginButton().click();
  }
}

export default new LoginPage();

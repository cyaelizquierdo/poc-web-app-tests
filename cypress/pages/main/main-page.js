class MainPage {
  constructor() {
    this.url = '/';
  }

  // Elements
  mainContent() { return cy.get('div.main-content'); }

  visit() {
    cy.visit(this.url);
  }

  shouldBeDisplay() {
    cy.url().should('eq', `${Cypress.config().baseUrl}${this.url}`);
    this.mainContent().should('be.visible');
  }
}

export default new MainPage();

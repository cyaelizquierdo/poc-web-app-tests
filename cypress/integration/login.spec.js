import loginPage from '../pages/login/login-page';
import mainPage from '../pages/main/main-page';

describe('Login page', () => {
  beforeEach(function () {
    cy.fixture('client-credentials').then((users) => { this.users = users; });
    cy.fixture('features/login/uris').then((uris) => { this.uris = uris; });
    loginPage.visit();
  });

  it('should display the ohana logo', function () {
    loginPage.ohanaLogo().imageShouldBeLoaded('/img/ohana-blue-logo.png');
  });

  it('should be able to login', function () {
    const { email, password } = this.users.loginUser;
    loginPage.login(email, password);
    // wait until login request is finished
    cy.intercept(this.uris.loginUri).as('login');
    cy.wait('@login');
    // check if the main page is displayed
    mainPage.shouldBeDisplay();
  });
});

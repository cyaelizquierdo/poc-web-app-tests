describe('Base url', () => {
  beforeEach(function () {
    cy.visit('/');
  });

  it('should redirect to the login page', function () {
    cy.url().should('eq', `${Cypress.config().baseUrl}/login`);
  });
});
